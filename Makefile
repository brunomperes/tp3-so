# Especifica o compilador
CC = gcc

# Especifica as opções do compilador, habilita aviso sobre erros
CFLAGS = -Wall -pg -g3

# Comando terminal para limpar sem confirmação
RM = rm -f

# Lista dos ficheiros (arquivos) objetos necessários para o programa final
OBJS = main.o arquivo.o fifo.o fila.o lista.o lru.o node.o random.o saida.o

# Nome do executável
MAIN = tp3virtual
 
# Argumentos para execução
ARQUIVO_ENTRADA = simulador.log
LRU = lru 
FIFO = fifo 
RANDOM = random 
PAGINA_CONSTANTE = 4
MEMORIA_CONSTANTE = 2048

# Modo de depuração (utilize 'd' sem aspas para habilitar)
DEBUG = d

# Opções para execução do Valgrind
VOPS = --leak-check=full --show-reachable=yes

# Compilação do programa e passos das ligações de dependências
$(MAIN): $(OBJS)
	@echo ""
	@echo " --- COMPILANDO PROGRAMA ---"
	@$(CC) $(CFLAGS) $(OBJS) -lm -o $(MAIN) 
	@echo ""

%.o: %.c %.h
	@echo ""
	@echo " --- COMPILANDO OBJETO \"$@\""
	@$(CC) $(CFLAGS) $< -c 

clean:
	$(RM) $(MAIN) *.o
	$(RM) gmon.out

run: $(MAIN)
	@echo "\nExecutando para página constante e memória crescente\n"
	@for tam_memoria in 64 128 256 512 1024 2048 4096 8192 16384 ; do \
		./$(MAIN) $(LRU) $(ARQUIVO_ENTRADA) $(PAGINA_CONSTANTE) $$tam_memoria $(DEBUG); \
		./$(MAIN) $(FIFO) $(ARQUIVO_ENTRADA) $(PAGINA_CONSTANTE) $$tam_memoria $(DEBUG); \
		./$(MAIN) $(RANDOM) $(ARQUIVO_ENTRADA) $(PAGINA_CONSTANTE) $$tam_memoria $(DEBUG); \
		echo ""; \
	done
	@echo "\nExecutando para página crescente e memória constante\n"
	@for tam_pagina in 2 4 8 16 32 64 ; do \
		./$(MAIN) $(LRU) $(ARQUIVO_ENTRADA) $$tam_pagina $(MEMORIA_CONSTANTE) $(DEBUG); \
		./$(MAIN) $(FIFO) $(ARQUIVO_ENTRADA) $$tam_pagina $(MEMORIA_CONSTANTE) $(DEBUG); \
		./$(MAIN) $(RANDOM) $(ARQUIVO_ENTRADA) $$tam_pagina $(MEMORIA_CONSTANTE) $(DEBUG); \
		echo ""; \
	done
	
valgrind: $(MAIN)
	valgrind $(VOPS) ./$(MAIN) $(LRU) $(ARQUIVO_ENTRADA) $(PAGINA_CONSTANTE) $(MEMORIA_CONSTANTE)

gprof: $(MAIN)
	./$(MAIN) $(RANDOM) $(ARQUIVO_ENTRADA) $(PAGINA_CONSTANTE) 128
	gprof -b -p $(MAIN)
