#include "saida.h"

#define NOME_ARQ_SAIDA_FIFO "resultado_fifo.txt"
#define NOME_ARQ_SAIDA_LRU "resultado_lru.txt"
#define NOME_ARQ_SAIDA_RANDOM "resultado_random.txt"

void escreveResultadoAlgoritmo(int algoritmoInt, int tamanhoBytesMemoria, int tamanhoBytesPagina, int paginasLidas, int paginasEscritas, int totalAcessos){

    FILE *output;

    if(algoritmoInt == FIFO) {
        output = fopen(NOME_ARQ_SAIDA_FIFO, "a");
        fprintf (output, "%s", "fifo\t");
    } else if(algoritmoInt == LRU) {
        output = fopen(NOME_ARQ_SAIDA_LRU, "a");
        fprintf (output, "%s", "lru\t");
    } else if(algoritmoInt == RANDOM) {
        output = fopen(NOME_ARQ_SAIDA_RANDOM, "a");
        fprintf (output, "%s", "random\t");
    }

    fprintf (output, "%d\t%d\t%d\t%d\t%d\n", tamanhoBytesPagina, tamanhoBytesMemoria, paginasLidas, paginasEscritas, totalAcessos);

    fclose(output);
}
