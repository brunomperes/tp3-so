#include "lru.h"
#include "saida.h"

extern int paginasLidas;
extern int paginasEscritas;

extern tabela tab;
extern bool debug;

void lru(FILE *input, int tamanhoBytesPagina, int tamanhoBytesMemoria) {

    List list;
    typeKey x;

    int numeroQuadros;
    int page;

    numeroQuadros = tamanhoBytesMemoria / tamanhoBytesPagina;

    tab = tabelaPaginas(tamanhoBytesPagina);

    createList(&list, numeroQuadros);

    paginasLidas = 0, paginasEscritas = 0;

    unsigned int s;
    unsigned int tmp;
    /*derivar o valor de s: */
    //int contador = 0;
    unsigned int addr;
    char rw;
    int totalAcessos = 0;

    while(!feof(input)) {

        if (fscanf(input, "%x %c", &addr, &rw) < 0){
            break;
        }
        totalAcessos++;

        tmp = tamanhoBytesPagina * pow(2,10);
        s = 0;
        while(tmp > 1) {
            tmp = tmp >> 1;
            s++;
        }

        page = addr >> s;
        x = page;

        if(find(x, &list) == false) {
            if(list.size < list.maxSize) {
                pushFront(x, &list);
                atualizaTabela(page, list.idContador);
                list.idContador++;
                if(rw == 'R') {
                    paginasLidas++;
                } else if(rw == 'W') {
                    tab.v[page].escrita = 1;
                    paginasLidas++;
                } else {
                    perror("ERRO NO ARQUIVO!");
                }
            } else {
                popBack(&list);
                pushFront(x, &list);
                atualizaTabela(page, list.idContador);
                list.idContador++;
                if(rw == 'R') {
                    paginasLidas++;
                } else if(rw == 'W') {
                    tab.v[page].escrita = 1;
                    paginasLidas++;
                } else {
                    perror("ERRO NO ARQUIVO!");
                }
            }
        } else {
            erase(x, &list);
            pushFront(x, &list);
            list.idContador++;
            if(rw == 'W') {
                tab.v[page].escrita = 1;
            }
        }
    }

    printf("Executando o simulador...\n");
    //printf("Arquivo de entrada: %s\n", input);
    printf("Tamanho da memoria: %d KB\n", tamanhoBytesMemoria);
    printf("Tamanho das paginas: %d KB\n", tamanhoBytesPagina);
    printf("Tecnica de reposicao: lru\n");

    printf("Numero de paginas lidas: %d\n", paginasLidas);
    printf("Numero de paginas escritas: %d\n", paginasEscritas);

    if (debug){
        escreveResultadoAlgoritmo(LRU, tamanhoBytesMemoria, tamanhoBytesPagina, paginasLidas, paginasEscritas, totalAcessos);
    }

}
