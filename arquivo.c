#include "arquivo.h"
#include <string.h>

extern tabela tab;

int paginasLidas, paginasEscritas;

int tamanhoTotalFuncaoRandom;

void leArquivo(FILE *input, int tamanhoBytesPagina, int tamanhoBytesMemoria, int algoritmoInt) {

    if(algoritmoInt == FIFO) {
        fifo(input, tamanhoBytesPagina, tamanhoBytesMemoria);
        // Imprime(fila);
    } else if(algoritmoInt == LRU) {
        lru(input, tamanhoBytesPagina, tamanhoBytesMemoria);
    } else if(algoritmoInt == RANDOM) {
        algRandom(input, tamanhoBytesPagina, tamanhoBytesMemoria);
    } else {
        printf("Algoritmo digitado na entrada invalido");
        exit(1);
    }
}

int converteTipoAlgoritmo(char *tipoAlgoritmo) {

    char algoritmoFIFO[5] = {"fifo"};
    char algoritmoLRU[4] = {"lru"};
    char algoritmoRANDOM[7] = {"random"};

    if(strcmp(tipoAlgoritmo, algoritmoFIFO) == 0) {
        return FIFO;
    } else if(strcmp(tipoAlgoritmo, algoritmoLRU) == 0) {
        return LRU;
    } else if(strcmp(tipoAlgoritmo, algoritmoRANDOM) == 0) {
        return RANDOM;
    }
    perror("ERRO: Algoritmo digitado invalido!\n\n");
    exit(1);
}

//Retorna uma tabela de páginas inicializada com valores padrão (-1)
tabela tabelaPaginas(int tamanhoPagina) {

    int aux = pow(2,10)*tamanhoPagina;

    int tamanhoTabela = 0;

    int contador = 0;

    int i;

    int bitsIdentificaoPagina;

    while(aux > 1) {
        aux = aux / 2;
        contador++;
    }

    bitsIdentificaoPagina = 32 - contador;

    tamanhoTabela = (pow(2, bitsIdentificaoPagina));

    tab.v = (quadroTabela*)malloc(tamanhoTabela * sizeof(quadroTabela));

    // Inicializa a tabela com um valor padrão
    for(i = 0 ; i <= tamanhoTabela ; i++) {
        tab.v[i].id = -1;
    }

    tamanhoTotalFuncaoRandom = tamanhoTabela - 1;

    return tab;
}

void atualizaTabela(int page, int id) {
    tab.v[page].id = id;
}
