#ifndef SAIDA_H_INCLUDED
#define SAIDA_H_INCLUDED

#include "arquivo.h"

void escreveResultadoAlgoritmo(int algoritmoInt, int tamanhoBytesMemoria, int tamanhoBytesPagina, int paginasLidas, int paginasEscritas, int totalAcessos);

#endif // SAIDA_H_INCLUDED
