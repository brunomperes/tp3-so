#ifndef LRU_H_INCLUDED
#define LRU_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include "arquivo.h"
#include "lista.h"

void lru(FILE *input, int tamanhoBytesPagina, int tamanhoBytesMemoria);

#endif // LRU_H_INCLUDED
