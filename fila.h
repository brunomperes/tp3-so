#ifndef FILA_H_INCLUDED
#define FILA_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
/*
Módulo que contém o TAD fila
com suas respectivas estruturas e funções
*/

typedef struct quadroTabela {
    int id;
    int escrita;
} quadroTabela;

typedef struct tabela {
    quadroTabela *v;
} tabela;

typedef struct TipoCelula *TipoApontador;

typedef int TipoChave;

//estrutura do TipoItem que contem a chave(valor) da fila.
typedef struct TipoItem {
    TipoChave Chave;
} TipoItem;

/*
Estrutura do TipoCelula que contem um tipoItem e um tipoApontador
prox que aponta para o próximo da fila.
*/
typedef struct TipoCelula {
    TipoItem Item;
    int id;
    TipoApontador Prox;
} TipoCelula;

/*
Estrutura do TipoFila que cria uma fila com um Tipo Apontador
para apontar para frente e tras na fila, maxElementos para o tamanho
da fila, e qntElementos para analisar se a fila ainda
tem espaço para inserção de mais alguma página.
*/
typedef struct TipoFila {
    TipoApontador Frente, Tras;
    int idContador;
    int maxElementos;
    int qntElementos;
} TipoFila;

/*
Cria uma fila vazia com o tamanho size ja definido
que no caso size será o tamanho de bytes da memória fisica
dividido pelo número de bytes por página.
*/
void FFVazia(TipoFila *Fila, int filaSize);

/*
Função para enfileirar a página na fila caso
haja espaço na fila. Caso não haja espaço
então desenfileira a fila e enfileira
a nova página.
*/
void Enfileira(TipoItem x, TipoFila *Fila, int id);

void Imprime(TipoFila Fila);

/*
Função para desenfileirar a página da fila
*/
void Desenfileira(TipoFila *Fila, TipoItem *Item);

/*
Função para pesquisar se a página desejada
está na fila, se sim retorna true,
se não estiver retornar false
*/
int pesquisaFila(int x, TipoFila *Fila);



#endif // FILA_H_INCLUDED
