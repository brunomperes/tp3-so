#include "lista.h"

extern int paginasEscritas;
extern tabela tab;
extern int tamanhoTotalFuncaoRandom;

void ListConstructor(List *list, int maxSize) {
    list->end = NewSentinel();
    list->end->ID = -1;
    list->size = 0;
    list->maxSize = maxSize;
    list->idContador = 0;
}

void createList(List *list, int maxSize) {
    ListConstructor(list, maxSize);

}

node *front(node *listEnd) {
    return listEnd->next;
}

node *back(node *listEnd) {
    return listEnd->prev;
}

int find(typeKey ID, List *list) {
    node *aux;

    for (aux = front(list->end); aux != list->end; aux = aux->next) {
        if (ID == aux->ID) {
            return true;
        }
    }

    return false;
}

void erase(typeKey x, List *list) {
    node *aux;
    aux = front(list->end);

    while(x != aux->ID) {
        aux = aux->next;
    }

    aux->next->prev = aux->prev;
    aux->prev->next = aux->next;
    free(aux);

    list->size--;
}

int eraseRandom(typeKey x, List *list){
    node *aux;
    aux = front(list->end);

    typeKey contador = 0;
    int idPagina;

    while(contador != x){
        aux = aux->next;
        contador++;
    }

    idPagina = aux->ID;

    aux->next->prev = aux->prev;
    aux->prev->next = aux->next;
    free(aux);

    list->size--;

    return idPagina;
}

void pop(List* list, int numeroQuadros) {
    typeKey x;
    x = (random() % numeroQuadros);

    int idPagina;

    idPagina = eraseRandom(x, list);

    if(tab.v[idPagina].escrita == 1) {
        paginasEscritas++;
        tab.v[idPagina].escrita = 0;
    }

    tab.v[x].id = -1;
}

void popBack(List* list) {
    node *aux = back(list->end);

    if(tab.v[aux->ID].escrita == 1) {
        paginasEscritas++;
        tab.v[aux->ID].escrita = 0;
    }

    tab.v[aux->ID].id = -1;

    aux->next->prev = aux->prev;
    aux->prev->next = aux->next;

    free(aux);
    list->size--;
}

void pushFront(typeKey ID, List *list) {
    node *aux = NewNode(ID, list->end, front(list->end));
    front(list->end)->prev = aux;
    list->end->next = aux;
    list->size++;
}
