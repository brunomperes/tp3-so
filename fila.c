#include "fila.h"

extern int paginasEscritas;
extern tabela tab;

void FFVazia(TipoFila *Fila, int filaSize) {
    Fila->Frente = (TipoApontador) malloc(sizeof(TipoCelula));
    Fila->Tras = Fila->Frente;
    Fila->Frente->Prox = NULL;
    Fila->maxElementos = filaSize;
    Fila->qntElementos = 0;
    Fila->idContador = 0;
}

void Enfileira(TipoItem x, TipoFila *Fila, int id) {
    Fila->Tras->Prox = (TipoApontador) malloc(sizeof(TipoCelula));
    Fila->Tras = Fila->Tras->Prox;
    Fila->Tras->Item = x;
    Fila->Tras->id = id;
    Fila->Tras->Prox = NULL;
    /* printf("Enfileirou: %d", Fila->Tras->Item.Chave);
     getchar();*/
}

void Desenfileira(TipoFila *Fila, TipoItem *Item) {
    TipoApontador q;

    q = Fila->Frente;

    if(tab.v[Fila->Frente->Item.Chave].escrita == 1) {
        paginasEscritas++;
        tab.v[Fila->Frente->Item.Chave].escrita = 0;
    }
    Fila->Frente = Fila->Frente->Prox;
    *Item = Fila->Frente->Item;

    /* printf("Desenfileirou %d\n", q->Prox->Item.Chave);
         getchar();*/

    tab.v[q->Prox->Item.Chave].id = -1;

    free(q);
}

void Imprime(TipoFila Fila) {
    TipoApontador Aux;
    Aux = Fila.Frente->Prox;
    printf("\nFILAAAAA:\n");
    while(Aux != NULL) {
        printf("%d\n", Aux->Item.Chave);
        Aux = Aux->Prox;
    }
}

int pesquisaFila(int x, TipoFila *Fila) {
    TipoApontador Aux;
    Aux = Fila->Frente->Prox;
    while(Aux != NULL) {
        if(x == Aux->Item.Chave) {
            return true;
        }
        Aux = Aux->Prox;
    }
    return false;
}
