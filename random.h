#ifndef RANDOM_H_INCLUDED
#define RANDOM_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include "lista.h"
#include "arquivo.h"

void algRandom(FILE *input, int tamanhoBytesPagina, int tamanhoBytesMemoria);

#endif // RANDOM_H_INCLUDED
