#include "fifo.h"
#include "saida.h"

extern int paginasLidas;
extern int paginasEscritas;

extern tabela tab;
extern bool debug;

void fifo(FILE* input, int tamanhoBytesPagina, int tamanhoBytesMemoria) {

    TipoFila fila;
    TipoItem item;

    int numeroQuadros;
    int page;

    numeroQuadros = tamanhoBytesMemoria / tamanhoBytesPagina;

    tab = tabelaPaginas(tamanhoBytesPagina);

    FFVazia(&fila, numeroQuadros);


    paginasLidas = 0, paginasEscritas = 0;

    unsigned int s;
    unsigned int tmp;
    unsigned int addr;
    char rw;
    int totalAcessos = 0;

    while(!feof(input)) {

        if (fscanf(input, "%x %c", &addr, &rw) < 0){
            break;
        }
        totalAcessos++;

        /*derivar o valor de s: */
        tmp = tamanhoBytesPagina * pow(2,10);
        s = 0;
        while(tmp > 1) {
            tmp = tmp >> 1;
            s++;
        }

        page = addr >> s;

        // Se página não está na memória
        if(tab.v[page].id == -1) {
            if(fila.qntElementos < fila.maxElementos) {
                item.Chave = page;
                Enfileira(item, &fila, fila.idContador);
                atualizaTabela(page, fila.idContador);
                fila.idContador++;
                fila.qntElementos++;
                if(rw == 'R') {
                    paginasLidas++;
                } else if(rw == 'W') {
                    tab.v[page].escrita = 1;
                    paginasLidas++;
                } else {
                    perror("ERRO NO ARQUIVO!");
                }
            } else {
                Desenfileira(&fila, &item);
                item.Chave = page;
                Enfileira(item, &fila, fila.idContador);
                atualizaTabela(page, fila.idContador);
                fila.idContador++;
                if(rw == 'R') {
                    paginasLidas++;
                } else if(rw == 'W') {
                    tab.v[page].escrita = 1;
                    paginasLidas++;
                } else {
                    perror("ERRO NO ARQUIVO!");
                }
            }
        } else {
            if(rw == 'W') {
                tab.v[page].escrita = 1;
            }
            fila.idContador++;
        }
    }
    printf("Executando o simulador...\n");
    //printf("Arquivode entrada: %s\n", input);
    printf("Tamanho da memoria: %d KB\n", tamanhoBytesMemoria);
    printf("Tamanho das paginas: %d KB\n", tamanhoBytesPagina);
    printf("Tecnica de reposicao: fifo\n");

    printf("Numero de paginas lidas: %d\n", paginasLidas);
    printf("Numero de paginas escritas: %d\n", paginasEscritas);

    if (debug){
        escreveResultadoAlgoritmo(FIFO, tamanhoBytesMemoria, tamanhoBytesPagina, paginasLidas, paginasEscritas, totalAcessos);
    }

}
