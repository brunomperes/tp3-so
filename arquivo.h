#ifndef ARQUIVO_H_INCLUDED
#define ARQUIVO_H_INCLUDED

#include "fifo.h"
#include "lru.h"
#include "random.h"
#include "math.h"


#define FIFO 0
#define LRU 1
#define RANDOM 2

#define R 0
#define W 1

//Função para ler o arquivo e chamar as funções de reposições de páginas: fifo, lru, lfu.
void leArquivo(FILE *input, int tamanhoBytesPagina, int tamanhoBytesMemoria, int algoritmoInt);

int converteTipoAlgoritmo(char *tipoAlgoritmo);

tabela tabelaPaginas(int tamanhoPagina);

void atualizaTabela(int page, int id);

#endif // ARQUIVO_H_INCLUDED
