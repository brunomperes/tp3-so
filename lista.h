#ifndef LISTA_H_INCLUDED
#define LISTA_H_INCLUDED

#include "node.h"
#include "fila.h"
/*
Módulo que contém o TAD de listas
e suas respectivas funções para se trabalhar no LRU E LFU
*/

typedef struct List {
    node *end;    // Nó marcador do fim da lista.
    int size;     // Número de elementos da lista.
    int maxSize;
    int idContador;
} List;

// Inicializa uma lista vazia com o nó end.
void ListConstructor(List *list, int maxSize);

// Cria uma lista com n elementos em ordem crescente, tal que n = listSize.
void createList(List *list, int maxSize);

void pop(List* list, int numeroQuadros);

// Retorna um ponteiro para página dentro da lista em O(1).
node *front(node *listEnd);

//Retorna um ponteiro para a ultima página dentro da lista em O(1).
node *back(node *listEnd);

// Retorna true se a página está na lista, e falso se a página não está.
int find(typeKey ID, List *list);

// Remove a página da lista.
void erase(typeKey x, List *list);

//Implementa o acesso da página.
void implementaAcesso(typeKey x, List* list);

//Remove a página com menos acesso da lista.
void removeMenorAcesso(List *list);

// Remove o ultimo elemento da lista em O(1).
void popBack(List *list);

// Insere um vértice no início da lista em O(1).
void pushFront(typeKey ID, List *list);


#endif // LISTA_H_INCLUDED
