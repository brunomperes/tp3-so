#include <stdio.h>
#include <stdlib.h>
#include "arquivo.h"
#include "fila.h"

/*
Passado os argumentos dos arquivos para a chamada da função
que lê os dados do arquivo e resolve o sistema de memória virtual.
*/

tabela tab;
bool debug;

int main(int argc, char *argv[]) {
    FILE *input;
    char *algoritmo;
    debug = false;

    int tamanhoBytesPagina, tamanhoBytesMemoria, algoritmoInt;

    if (argc < 5) {
        perror("[ERRO] Quantidade de argumentos invalida, ao menos 4 argumentos devem ser fornecidos!");
        exit(1);
    }

    if (argc > 5) {
        if (argv[5][0] == 'd'){
            debug = true;
        }
    }

    algoritmo = argv[1];
    input = fopen(argv[2], "r");
    tamanhoBytesPagina = atoi(argv[3]);
    tamanhoBytesMemoria = atoi(argv[4]);

    if (tamanhoBytesPagina > tamanhoBytesMemoria){
        printf("[ERRO] O tamanho da pagina (%d) e maior que o da memoria (%d)", tamanhoBytesPagina, tamanhoBytesMemoria);
        exit(1);
    }

    algoritmoInt = converteTipoAlgoritmo(algoritmo);

    leArquivo(input, tamanhoBytesPagina, tamanhoBytesMemoria, algoritmoInt);

    fclose(input);

    return 0;
}
