#ifndef FIFO_H_INCLUDED
#define FIFO_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include "fila.h"
#include "arquivo.h"

void fifo(FILE* input, int tamanhoBytesPagina, int tamanhoBytesMemoria);

#endif // FIFO_H_INCLUDED
