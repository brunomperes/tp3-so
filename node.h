#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
/*
Modulo que contem a estrutura nó
que preenche a lista e suas respectivas
funções.
*/
typedef int typeKey;

typedef struct Node {
    typeKey ID;             // Contém o ID da página
    struct Node *next;  // Apontador para o próximo nó.
    struct Node *prev;  // Apotador para o nó anterior.
    int acessos;        //váriavel para calcular o número de acessos da página usado no lfu.
} node;

// Cria o nó sentinela.
node *NewSentinel();

// Cria um novo node.
node *NewNode(typeKey ID, node *left, node *right);


#endif // NODE_H_INCLUDED
